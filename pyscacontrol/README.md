
## Simple Python GTK pysca GUI application to control Visca cameras

depends on pyserial:

    sudo pip install pyserial

Superuser privileges might be necessary to access the serial device or add the user to the group 'dialout' then restart:

    sudo usermod -a -G dialout <username>

the serial port and movement speed may be defined as variables

the power_on command may fail if it goes over the pysca timeout the default seems to be OK (10s)