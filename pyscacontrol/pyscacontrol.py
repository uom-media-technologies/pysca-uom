# Copyright 2014 Andrew Wilson

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

""" Simple Python GTK pysca GUI application to control Visca camera
"""
import os
import sys
import gtk
import time

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))
import pysca.pysca as pysca

# This is the default Visca device this plugin talks to
DEFAULT_DEVICE = 1

# This is the path to the serial visca device
PORT = "/dev/ttyUSB0"

# Set Movement speed here 1 to 7
SPEED = 5

class SimPyViscaControl:

    def destroy(self, widget, data=None):
        gtk.main_quit()

    def power_on(self, widget):
        try:
            pysca.connect(PORT)
            pysca.set_power_on(DEFAULT_DEVICE, True)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def power_off(self, widget):
        try:
            pysca.set_power_on(DEFAULT_DEVICE, False)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def reset(self, widget):
        try:
            pysca.pan_tilt_reset(DEFAULT_DEVICE)
            pysca.clear_all()

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def pan_right(self, widget, event):
        try:
            pysca.pan_tilt(DEFAULT_DEVICE, SPEED)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def pan_left(self,widget, event):
        try:
            pysca.pan_tilt(DEFAULT_DEVICE, -SPEED)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def pan_stop(self, widget, event):
        try:
            pysca.pan_tilt(DEFAULT_DEVICE, 0)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def tilt_up(self, widget, event):
        try:
            pysca.pan_tilt(DEFAULT_DEVICE, None, SPEED)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def tilt_down(self,widget, event):
        try:
            pysca.pan_tilt(DEFAULT_DEVICE, None, -SPEED)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def tilt_stop(self, widget, event):
        try:
            pysca.pan_tilt(DEFAULT_DEVICE, None, 0)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def zoom_in(self, widget, event):
        try:
            pysca.zoom(DEFAULT_DEVICE, 'tele', SPEED)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def zoom_out(self, widget, event):
        try:
            pysca.zoom(DEFAULT_DEVICE, 'wide', SPEED)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def zoom_stop(self, widget, event):
        try:
            pysca.zoom(DEFAULT_DEVICE, 'stop')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def brightness_up(self, widget):
        try:
            pysca.set_brightness(DEFAULT_DEVICE, 'up')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def brightness_down(self, widget):
        try:
            pysca.set_brightness(DEFAULT_DEVICE, 'down')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def b_ae_mode(self, widget):
        try:
            pysca.set_ae_mode(DEFAULT_DEVICE, 'bright')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def exp_comp_up(self, widget):
        try:
            pysca.set_exp_comp(DEFAULT_DEVICE, 'up')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def exp_comp_down(self, widget):
        try:
            pysca.set_exp_comp(DEFAULT_DEVICE, 'down')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def exp_comp_reset(self, widget):
        try:
            pysca.set_exp_comp(DEFAULT_DEVICE, 'reset')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def hr_mode_on(self, widget):
        try:
            pysca.set_hr_mode(DEFAULT_DEVICE, 'on')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def wb_onepush_mode(self, widget):
        try:
            pysca.set_wb_mode(DEFAULT_DEVICE, 'onepush')

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def wb_onepush_trigger(self, widget):
        try:
            pysca.trigger_wb(DEFAULT_DEVICE)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def get_hr_mode(self, widget):
        try:
            pysca.get_hr_mode(DEFAULT_DEVICE)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def set_memory(self, widget):
        try:
            pysca.set_memory(DEFAULT_DEVICE, 0)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def recall_memory(self, widget):
        try:
            pysca.recall_memory(DEFAULT_DEVICE, 0)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def set_nr(self, widget):
        try:
            pysca.set_nr(DEFAULT_DEVICE, 3)

        except Exception as e:
            print("Error in device {}: {}".format(PORT, e))

    def create_pan_tilt_button(self, movement, arrow_type):
        pt_button = gtk.Button()
        arrow = gtk.Arrow(arrow_type, gtk.SHADOW_NONE)
        pt_button.add(arrow)
        pt_button.connect('button-press-event', movement)
        pt_button.connect('button-release-event', self.pan_stop)
        return pt_button

    def __init__(self):
        self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        self.window.set_position(gtk.WIN_POS_CENTER)
        self.window.set_size_request(600, 400)
        self.window.set_title('simple pysca controls')
        self.tooltips = gtk.Tooltips()

        self.on_button = gtk.Button('ON')
        self.on_button.connect('clicked', self.power_on)

        self.off_button = gtk.Button('OFF')
        self.off_button.connect('clicked', self.power_off)

        self.pan_right_button = self.create_pan_tilt_button(self.pan_right, gtk.ARROW_RIGHT)
        self.tooltips.set_tip(self.pan_right_button, 'click to pan right\nrelease to stop')

        self.pan_left_button = self.create_pan_tilt_button(self.pan_left, gtk.ARROW_LEFT)
        self.tooltips.set_tip(self.pan_left_button, 'click to pan left\nrelease to stop')

        self.tilt_up_button = self.create_pan_tilt_button(self.tilt_up, gtk.ARROW_UP)
        self.tooltips.set_tip(self.tilt_up_button, 'click to tilt up\nrelease to stop')

        self.tilt_down_button = self.create_pan_tilt_button(self.tilt_down, gtk.ARROW_DOWN)
        self.tooltips.set_tip(self.tilt_down_button, 'click to tilt down\nrelease to stop')

        self.zoom_in_button = gtk.Button('zoom in')
        self.zoom_in_button.connect('button-press-event', self.zoom_in)
        self.zoom_in_button.connect('button-release-event', self.zoom_stop)

        self.zoom_out_button = gtk.Button('zoom out')
        self.zoom_out_button.connect('button-press-event', self.zoom_out)
        self.zoom_out_button.connect('button-release-event', self.zoom_stop)

        self.reset_button = gtk.Button('reset')
        self.reset_button.connect('clicked', self.reset)

        self.mem_button = gtk.Button('set memory 1')
        self.mem_button.connect('clicked', self.set_memory)

        self.recmem_button = gtk.Button('recall mem1')
        self.recmem_button.connect('clicked', self.recall_memory)

        self.bup_button = gtk.Button('brightness up')
        self.bup_button.connect('clicked', self.brightness_up)

        self.bdwn_button = gtk.Button('brightness down')
        self.bdwn_button.connect('clicked', self.brightness_down)

        self.b_ae_button = gtk.Button('set bright AE mode')
        self.b_ae_button.connect('clicked', self.b_ae_mode)

        self.expcomp_up_button = gtk.Button('exp comp up')
        self.expcomp_up_button.connect('clicked', self.exp_comp_up)

        self.expcomp_dwn_button = gtk.Button('exp comp down')
        self.expcomp_dwn_button.connect('clicked', self.exp_comp_down)

        self.expcomp_rst_button = gtk.Button('exp comp reset')
        self.expcomp_rst_button.connect('clicked', self.exp_comp_reset)

        self.hrmode_on_button = gtk.Button('high res on')
        self.hrmode_on_button.connect('clicked', self.hr_mode_on)

        self.nr_button = gtk.Button('noise to 3')
        self.nr_button.connect('clicked', self.set_nr)

        self.wb_onepush_button = gtk.Button('white balance onepush')
        self.wb_onepush_button.connect('clicked', self.wb_onepush_mode)

        self.wb_onepush_trigger_button = gtk.Button('onepush trigger')
        self.wb_onepush_trigger_button.connect('clicked', self.wb_onepush_trigger)

        self.get_hr_mode_button = gtk.Button('get high res setting')
        self.get_hr_mode_button.connect('clicked', self.get_hr_mode)

        self.box_row_0 = gtk.HBox()
        self.box_row_0.pack_start(self.pan_left_button)
        self.box_row_0.pack_start(self.reset_button)
        self.box_row_0.pack_start(self.mem_button)
        self.box_row_0.pack_start(self.recmem_button)
        self.box_row_0.pack_start(self.pan_right_button)
        self.box_row_0.pack_start(self.b_ae_button)
        self.box_row_0.pack_start(self.expcomp_rst_button)

        self.box_row_1 = gtk.HBox()
        self.box_row_1.pack_start(self.tilt_up_button)
        self.box_row_1.pack_start(self.tilt_down_button)

        self.box_row_2 = gtk.HBox()
        self.box_row_2.pack_start(self.on_button)
        self.box_row_2.pack_start(self.off_button)
        self.box_row_2.pack_start(self.bup_button)
        self.box_row_2.pack_start(self.bdwn_button)
        self.box_row_2.pack_start(self.expcomp_up_button)
        self.box_row_2.pack_start(self.expcomp_dwn_button)

        self.box_row_4 = gtk.HBox()
        self.box_row_4.pack_start(self.hrmode_on_button)
        self.box_row_4.pack_start(self.get_hr_mode_button)
        self.box_row_4.pack_start(self.nr_button)
        self.box_row_4.pack_start(self.wb_onepush_button)
        self.box_row_4.pack_start(self.wb_onepush_trigger_button)

        self.box_row_3 = gtk.VBox()
        self.box_row_3.pack_start(self.box_row_1)
        self.box_row_3.pack_start(self.box_row_0)
        self.box_row_3.pack_start(self.zoom_in_button)
        self.box_row_3.pack_start(self.zoom_out_button)
        self.box_row_3.pack_start(self.box_row_2)
        self.box_row_3.pack_start(self.box_row_4)



        self.window.add(self.box_row_3)
        self.window.show_all()
        self.window.connect('destroy', self.destroy)
        self.power_on(widget=None)

    def main(self):
        gtk.main()

if __name__ == '__main__':
    simpyviscacontrol = SimPyViscaControl()
    simpyviscacontrol.main()